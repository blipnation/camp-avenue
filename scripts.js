var masterTL = new TimelineMax({
  paused:true,
  delay:1,
  repeat:-1,
  yoyo:true
});

var dur = 0.4,
    dur2 = 0.5,
    easing = Power1.easeInOut,
    easing2 = Power1.easeInOut;

TweenMax.set(".dot", {
  transformStyle: "preserve-3d",
  transformOrigin: "50% 50%"
});

masterTL
.to("#blue", dur, {
  bezier: {
    curviness: 2,
    values: [{
      x: 0,
      y: 7
    }, {
      x: 0,
      y: 0
    }, {
      x: 25,
      y: -35
    }, {
      x: 53,
      y: 0
    }],
    autoRotate: true
  },
  ease: easing
},"blueJumpFirst")
.to("#red", dur2, {
  bezier: {
    curviness: 2,
    values: [{
      x: 5,
      y: 0
    },{
      x: -53,
      y: 0
    }],
    autoRotate: true
  },
  ease: easing2
},"blueJumpFirst-=0.1")
.to("#blue", dur, {
  bezier: {
    curviness: 2,
    values: [{
      x: 53,
      y: 7
    }, {
      x: 53,
      y: 0
    }, {
      x: 83,
      y: -35
    }, {
      x: 106,
      y: 0
    }],
    autoRotate: true
  },
  ease: easing
},"blueJumpTwo")

.to("#yellow", dur2, {
  bezier: {
    curviness: 2,
    values: [{
      x: 5,
      y: 0
    },{
      x: -53,
      y: 0
    }],
    autoRotate: true
  },
  ease: easing2
},"blueJumpTwo-=0.1")

.to("#blue", dur, {
  bezier: {
    curviness: 2,
    values: [{
      x: 106,
      y: 7
    }, {
      x: 106,
      y: 0
    }, {
      x: 140,
      y: -35
    }, {
      x: 160,
      y: 0
    }],
    autoRotate: true
  },
  ease: easing
},"blueJumpThree")

.to("#green", dur2, {
  bezier: {
    curviness: 2,
    values: [{
      x: 5,
      y: 0
    },{
      x: -53,
      y: 0
    }],
    autoRotate: true
  },
  ease: easing2
},"blueJumpThree-=0.07")/**/

.progress(1).progress(0)
.play();

window.onload = function(){
	var controller = new ScrollMagic.Controller()

	setTimeout(function() {
		masterTL.to('.loader', 1, {
			opacity: 0,
			onComplete: function() {
				masterTL.stop()
				$('.loader').css('display', 'none')

				$('.popup-trigger').trigger('click')
			}
		})
	}, 1)

	var gallery_slick = $('#gallery .images .slides').slick({
		asNavFor: '#gallery .images .slides-nav',
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		arrows: false,
	})

	var gallery_slick_nav = $('#gallery .images .slides-nav').slick({
		asNavFor: '#gallery .images .slides',
		slidesToShow: 5,
 		slidesToScroll: 1,
 		infinite: true,
		focusOnSelect: true,
		centerPadding: '14px',
		centerMode: true,
		centerPadding: '60px',
		nextArrow: '<i class="fas fa-chevron-right"></i>',
		prevArrow: '<i class="fas fa-chevron-left"></i>',
		responsive: [
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 1,
				}
		    },
		]
	})

	var rates_slick = $('#rates .prices').slick({
		slidesToShow: 2,
 		slidesToScroll: 1,
 		infinite: true,
		nextArrow: '<i class="fas fa-chevron-right"></i>',
		prevArrow: '<i class="fas fa-chevron-left"></i>',
		responsive: [
		    {
				breakpoint: 640,
				settings: {
					slidesToShow: 1,
				}
		    },
		]
	})

	var promo = $('#promos-events .list').slick({
		slidesToShow: 2,
 		slidesToScroll: 1,
 		infinite: true,
		nextArrow: '<i class="fas fa-chevron-right"></i>',
		prevArrow: '<i class="fas fa-chevron-left"></i>',
		responsive: [
		    {
				breakpoint: 640,
				settings: {
					slidesToShow: 1,
				}
		    },
		]
	})

	var tween = new TimelineMax()

	tween.from('main .bg-image', 20, {
		scale: 1.6
	})
	.from('#home img', 1, {
		scale: 0.3,
		opacity: 0,
		y: '+=20',
		rotation: 960
	}, '-=19')
	.from('#home h1', 1, {
		y: '+=20',
		opacity: 0,
	}, '-=19')
	.from('#home h4', 1, {
		y: '+=20',
		opacity: 0,
	}, '-=18.7')
	.from('#home button', 1, {
		y: '+=20',
		opacity: 0,
	}, '-=18.4')

	var tween = new ScrollMagic.Scene({
		triggerElement:'#home',
	})
	.setTween(tween)
	.addTo(controller)

	var tween = new TimelineMax()

	tween.from('#about .list>div:nth-child(1)', 1, { 
			left:'+=200',
			top: '+=200',
			opacity: 0,
		},)
		.from('#about .list>div:nth-child(2)', 1, {
			top: '+=200',
			opacity: 0,
		}, '-=0.9')
		.from('#about .list>div:nth-child(3)', 1, {
			right: '+=200',
			top: '+=200',
			opacity: 0,
		}, '-=0.9')
		.from('#about .list>div:nth-child(4)', 1, { 
			left:'-=200',
			top: '-=200',
			opacity: 0,
		}, '-=0.9')
		.from('#about .list>div:nth-child(5)', 1, {
			top: '-=200',
			opacity: 0,
		}, '-=0.9')
		.from('#about .list>div:nth-child(6)', 1, {
			right: '-=200',
			top: '-=200',
			opacity: 0,
		}, '-=0.9')
		.from('#about h2', 1, {
			top: '+=100',
			opacity: 0,
		}, '-=1')
		.from('#about h4', 1, {
			top: '+=100',
			opacity: 0,
		}, '-=0.9')
		.from('#about p', 1, {
			top: '+=100',
			opacity: 0,
		}, '-=0.7')
		.from('#about button', 1, {
			scale: 0.9,
			opacity: 0, 
			ease: Bounce.easeOut
		}, '-=0.5')

	var tween = new ScrollMagic.Scene({
		triggerElement:'#about',
	})
	.setTween(tween)
	.addTo(controller)

	var tween = new TimelineMax()

	tween.staggerFrom('#gallery h2, #gallery h4, #gallery p, #gallery .images', 0.7, {
		top: '+=100',
		opacity: 0
	}, 0.3)

	var tween = new ScrollMagic.Scene({
		triggerElement:'#gallery'
	})
	.setTween(tween)
	.addTo(controller)

	var tween = new TimelineMax()

	tween.staggerFrom('#rates .copy h2, #rates .copy h4, #rates .copy p, #rates .prices', 0.7, {
		top: '+=100',
		opacity: 0
	}, 0.3)

	var tween = new ScrollMagic.Scene({
		triggerElement:'#rates'
	})
	.setTween(tween)
	.addTo(controller)

	var tween = new TimelineMax()

	tween.staggerFrom('#promos-events .copy h2, #promos-events .copy h4, #promos-events .copy p, #promos-events .list', 0.7, {
		top: '+=100',
		opacity: 0
	}, 0.3)

	var tween = new ScrollMagic.Scene({
		triggerElement:'#promos-events'
	})
	.setTween(tween)
	.addTo(controller)

	var tween = new TimelineMax()

	tween.from('#contact>h2', 1, {
			top: '+=100',
			opacity: 0
		})
		.staggerFrom("#contact .copy .content>div, #contact .details .content>div, #contact .mapouter", 0.7, {
			opacity: 0,
			y: 100
		}, 0.3)

	var tween = new ScrollMagic.Scene({
		triggerElement:'#contact'
	})
	.setTween(tween)
	.addTo(controller)

	var open_button = document.querySelector('nav .open')
	var close_button = document.querySelector('nav .close')
	var tween = new TimelineMax()

	close_button.addEventListener('click', function() {
		tween.reverse(0.6)
		document.querySelector('body').style.overflow = 'auto';
	})
	open_button.addEventListener('click', function() {
		document.querySelector('body').style.overflow = 'hidden';
		if (tween.reversed()) {
			tween.play()
			return
		}
		tween
		.to('nav .navigation', 0.3, {
			right: 0,
			opacity: 1
		})
		.from('nav .navigation .logo img', 0.3, {
			rotation: 360,
			y: -90,
			opacity: 0
		})
		.staggerFrom('nav .navigation ul li', 0.3, {
			opacity: 0,
			x: -100
		}, 0.1)
	})

	var menus = document.querySelectorAll('.navigation ul li');

	function goTo(elem) {
		var container = $('.viewport')
		var scrollTo = $(elem)
		
		container.animate({
			scrollTop: container.scrollTop() + (scrollTo.offset().top - container.offset().top)});
	}

	for (var i = menus.length - 1; i >= 0; i--) {
		menus[i].addEventListener('click', function() {
			close_button.click()

			goTo("#" + this.dataset.target)
		})
	}
	
	document.querySelector('#home .cta')
		.addEventListener('click', function() {
			goTo("#rates")
		})
}